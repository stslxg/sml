(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)
fun all_except_option (x, y) = 
    case y of 
	[] => NONE
      | yy :: y' => if same_string (x, yy) 
		    then SOME y'
		    else case all_except_option (x, y') of 
			     NONE => NONE
			   | SOME yy' => SOME (yy :: yy')

fun get_substitutions1 (x, s) = 
    case x of 
	[] => []
      | xx :: x' => let val t_ans = get_substitutions1 (x', s) 
		    in case all_except_option (s, xx) of 
			   NONE => t_ans
			 | SOME yy => yy @ t_ans
		    end

fun get_substitutions2 (x, s) = 
    let fun rev (x, y) = 
	    case x of 
		[] => y
	      | xx :: x' =>  rev (x', xx :: y) 
	fun aux (x, y) = 
	    case x of 
		[] => y
	      | xx :: x' => case all_except_option (s, xx) of 
				NONE => aux (x', y) 
			      | SOME yy => aux (x', rev(yy, []) @ y)
    in 
	rev (aux (x, []), [])
    end

fun similar_names (x, y) = 
    let val {first = x1, middle = x2, last = x3} = y
	val ans = get_substitutions2 (x, x1) 
	fun aux (l, yy) = 
	    case l of 
		[] => yy
	      | ll :: l' => aux (l', {first = ll, middle = x2, last = x3} :: yy)
	fun rev (x, y) = 
	    case x of 
		[] => y
	      | xx :: x' =>  rev (x', xx :: y) 
    in 
	rev (aux (ans, [y]), []) 
    end
(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)
fun card_color (s, r) = 
    case s of 
	Clubs => Black
      | Spades => Black 
      | Diamonds => Red
      | Hearts => Red

fun card_value (s, r) = 
    case r of 
	Num n => n
      | Ace => 11
      | _ => 10

fun remove_card (cs, c, e) = 
    case cs of 
	[] => raise e
      | cc :: cs' => if cc = c 
		     then cs'
		     else cc :: remove_card (cs', c, e) 
			  
fun all_same_color cs = 
    case cs of 
	[] => true
      | x :: [] => true
      | x :: y :: cs' => card_color x = card_color y andalso all_same_color (y :: cs')

fun sum_cards cs = 
    let fun aux (s, sum) = 
	    case s of 
		[] => sum
	      | cc :: s' => aux (s', card_value(cc) + sum) 
    in aux (cs, 0)
    end

fun score (cs, goal) = 
    let val sum = sum_cards cs
	val pre_score = if sum > goal
			then 3 * (sum - goal) 
			else goal - sum
    in if all_same_color cs
       then pre_score div 2
       else pre_score
    end 

fun officiate (cs, ms, goal) = 
    let fun go (cl, hl, ml) = 
	    case (ml, cl) of 
		([], _) => score (hl, goal)
	      | ((Discard c) :: ml', _) => go (cl, remove_card (hl, c, IllegalMove), ml')
	      | (Draw :: _, []) => score (hl, goal)
	      | (Draw :: ml', c :: cl') => let val hl' = c :: hl
					       val s = sum_cards hl'
					   in if s > goal 
					      then score (hl', goal)
					      else go (cl', hl', ml')
					   end  
    in go (cs, [], ms) 
    end  

fun sum_cards_challenge cs = 
    let fun aux (s, sum, count) = 
	    case s of 
		[] => (sum, count)
	      |  (_, Ace) :: s' => aux (s', 1 + sum, count + 1) 
	      | cc :: s' => aux (s', card_value(cc) + sum, count)
    in aux (cs, 0, 0)
    end

fun score_challenge (cs, goal) = 
    let val (sum, count) = sum_cards_challenge cs
	fun get_score s = 
	    let val pre_score = if s > goal
				then 3 * (s - goal) 
				else goal - s
	    in if all_same_color cs
	       then pre_score div 2
	       else pre_score
	    end 
	fun get_list (c, res) = 
	    if c = 0
	    then res
	    else get_list (c - 1, get_score (sum + c * 10) :: res)
	fun get_min l = 
	    case l of 
		ll :: [] => ll
	      | ll :: l' => Int.min(ll, get_min l')
    in get_min (get_list (count, [get_score(sum)]))
    end 

fun officiate_challenge (cs, ms, goal) = 
    let fun go (cl, hl, ml) = 
	    case (ml, cl) of 
		([], _) => score_challenge (hl, goal)
	      | ((Discard c) :: ml', _) => go (cl, remove_card (hl, c, IllegalMove), ml')
	      | (Draw :: _, []) => score_challenge (hl, goal)
	      | (Draw :: ml', c :: cl') => let val hl' = c :: hl
					       val (s, _) = sum_cards_challenge hl'
					   in if s > goal 
					      then score_challenge (hl', goal)
					      else go (cl', hl', ml')
					   end  
    in go (cs, [], ms) 
    end  

