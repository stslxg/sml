(* Coursera Programming Languages, Homework 3, Provided Code *)
exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(**** you can put all your code here ****)
val only_capitals =
    List.filter (fn x => Char.isUpper (String.sub (x, 0)))

val longest_string1 = 
    List.foldl (fn (x, acc) => if String.size x > String.size acc
			    then x
			    else acc)
	       ""

val longest_string2 = 
    List.foldl (fn (x, acc) => if String.size x >= String.size acc
			    then x
			    else acc)
	       ""

fun longest_string_helper predicate =
    List.foldl (fn (x, acc) => if predicate (String.size x, String.size acc)
			       then x
			       else acc)
	       ""

val longest_string3 = 
    longest_string_helper (fn (x, acc) => x > acc)

val longest_string4 = 
    longest_string_helper (fn (x, acc) => x >= acc)

val longest_capitalized = longest_string1 o only_capitals 

val rev_string = String.implode o List.rev o String.explode

fun first_answer f xs =
    case xs of 
	[] => raise NoAnswer
      | x :: xs' => case f x of 
			SOME v => v
		      | NONE => first_answer f xs'

fun all_answers f xs = 
    let fun all_acc xs acc = 
	    case xs of 
		[] => SOME acc
	      | x :: xs' => case f x of 
				NONE => NONE
			      | SOME lst => all_acc xs' (acc @ lst)
    in 
	all_acc xs []
    end 

val count_wildcards = g (fn () => 1) (fn x => 0)

val count_wild_and_variable_lengths = g (fn () => 1) (fn s => String.size s)

fun count_some_var (name, pattern) = g (fn () => 0) (fn s => if s = name 
							     then 1
							     else 0) 
				       pattern
val check_pat =
    let
	fun get_var p =
	    case p of
		Variable x        => [x]
	      | TupleP ps         => List.foldl (fn (p,acc) => (get_var p) @ acc) [] ps
	      | ConstructorP(_,p) => get_var p
	      | _                 => []
	fun has_repeated lst = 
	    let
		fun aux l =
		    case l of 
			[] => true
		      | var :: l' => not (List.exists (fn s => s = var) l') andalso aux l'
	    in aux lst
	    end
    in
	has_repeated o get_var 
    end

fun match (value, p) =
    if check_pat p
    then case (value, p) of
	     (_, Wildcard) => SOME []
	   | (_, Variable var) => SOME [(var, value)]
	   | (Unit, UnitP) => SOME []
	   | (Const x, ConstP y) => if x = y then SOME [] else NONE
	   | (Tuple vs, TupleP ps) =>
	     if List.length vs = List.length ps
	     then all_answers match (ListPair.zip (vs, ps))  
	     else NONE
	   | (Constructor(s2, v), ConstructorP(s1, p)) =>
	     if s1 = s2
	     then match(v, p)
	     else NONE
	   | _ => NONE
    else NONE

fun first_match v ps =
    SOME (first_answer (fn p => match (v, p)) ps)
    handle NoAnswer => NONE

